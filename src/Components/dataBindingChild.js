import React from 'react';
import TextField from '@material-ui/core/TextField';

class DataBindingChild extends React.Component {
    state = {
        dataFromParent: ''
    }
    constructor(props) {
        super(props);
    }
    handleDataSendToParent(e){
        this.props.handleDataFromChild(e.target.value)
    }
    render() {
        return (
            <div>
                <p>This is Child</p>
                <div className="inputContainer">
                    <TextField id="outlined-basic3" onChange={(event) => this.handleDataSendToParent(event)} label="Outlined" variant="outlined" />
                </div>

                <p>Parent Says {this.props.dataFromParent}</p>
            </div>
        )
    }
}

export default DataBindingChild;