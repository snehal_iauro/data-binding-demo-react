import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import React from 'react'
import '../Components/dataBinding.css';
import DataBindingChild from '../Components/dataBindingChild';
import DataBindingChildToParent from './dataBindingChildToParent';
import NestedLOneA from './NestesL1a';
import NestedLOneB from './NestedL1b';

class DataBinding extends React.Component {
    state = {
        oneWayTextFieldValue: '',
        oneWayTtextFieldValueToPara: '',
        twoWayTextFieldValue: '',
        parentTextBoxValue: '',
        childTextBoxValue: ''
    }
    displayTextFieldValue() {
        this.setState({ oneWayTtextFieldValueToPara: this.state.oneWayTextFieldValue })
    }
    handleOneWayTextFieldChange(e) {
        this.setState({ oneWayTextFieldValue: e.target.value });
    }
    handleTwoWayTextFieldChange(e) {
        this.setState({ twoWayTextFieldValue: e.target.value });
    }
    handleParentToChildData(e) {
        this.setState({ parentTextBoxValue: e.target.value });
    }

    handleDataFromChild(e) {
        console.log(e)
        this.setState({ childTextBoxValue: e })
    }
    render() {
        return (
            <div className="dataBindingContainer">
                <div className="oneWayDataBinding">
                    <h1>One way binding</h1>
                    <div className="inputContainer">
                        <TextField className="spaceBetween" id="outlined-basic1" value={this.state.oneWayTextFieldValue} onChange={(event) => this.handleOneWayTextFieldChange(event)} label="Outlined" variant="outlined" />
                        <Button variant="contained" color="primary" onClick={() => this.displayTextFieldValue()}>
                            Click to get Value
                        </Button>
                    </div>
                    <p>{this.state.oneWayTtextFieldValueToPara}</p>
                </div>

                <div className="oneWayDataBinding">
                    <h1>Two way binding</h1>
                    <div className="inputContainer">
                        <TextField className="spaceBetween" id="outlined-basic2" value={this.state.twoWayTextFieldValue} onChange={(event) => this.handleTwoWayTextFieldChange(event)} label="Outlined" variant="outlined" />
                        <TextField id="outlined-basic2" value={this.state.twoWayTextFieldValue} onChange={(event) => this.handleTwoWayTextFieldChange(event)} label="Outlined" variant="outlined" />
                    </div>
                    <p>{this.state.twoWayTextFieldValue}</p>
                </div>

                <div className="oneWayDataBinding">
                    <h1>Parent child communication</h1>
                    <p>This is Parent</p>
                    <div className="inputContainer">
                        <TextField id="outlined-basic3" onChange={(event) => this.handleParentToChildData(event)} label="Outlined" variant="outlined" />
                    </div>
                    <p>Child Says {this.state.childTextBoxValue}</p>
                    <DataBindingChild dataFromParent={this.state.parentTextBoxValue} handleDataFromChild={(e) => this.handleDataFromChild(e)}></DataBindingChild>
                    {/* <DataBindingChildToParent handleDataFromChild={(e) => this.handleDataFromChild(e)}></DataBindingChildToParent> */}
                </div>

                <div className="oneWayDataBinding">
                    <h1>No direct realtion component communication</h1>
                    <div className="noDirectRelationContainer">
                        <NestedLOneA />
                        <NestedLOneB />
                    </div>

                </div>


            </div>
        )
    }
}
export default DataBinding