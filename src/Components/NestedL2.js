import React from 'react';
import TextField from '@material-ui/core/TextField';
import { messageService, dataPassSubscriber } from '../Services/DataPassingService';

class NestedLTwo extends React.Component {
    constructor(props) {
        super(props);
    }
    handleDataSendToParent(e){
        dataPassSubscriber.next(e.target.value);
    }
    render() {
        return (
            <div className="NestedLTwoContainer">
                <p>Nested Level 2</p>
                <div className="inputContainer">
                    <TextField id="outlined-basic3" onChange={(event) => this.handleDataSendToParent(event)} label="Outlined" variant="outlined" />
                </div>
            </div>
        )
    }
}

export default NestedLTwo;