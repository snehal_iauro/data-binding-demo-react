import React from 'react';
import NestedLTwo from './NestedL2';

class NestedLOneA extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <div className="nestedLOneAContainer">
                <p>Nested Level 1A</p>
                <NestedLTwo />
            </div>
        )
    }
}

export default NestedLOneA;