import React from 'react';
import TextField from '@material-ui/core/TextField';
import { messageService, dataPassSubscriber } from '../Services/DataPassingService';

class NestedLOneB extends React.Component {
    state = {
        dataFromIndirectComponent : ''
    }
    constructor(props) {
        super(props);
        dataPassSubscriber.subscribe(data =>{
            this.setState({dataFromIndirectComponent:data})
        })
    }
    handleDataSendToParent(e){
        this.props.handleDataFromChild(e.target.value)
    }
    render() {
        return (
            <div className="nestedLOneBContainer">
                <p>Nested Level 1 B</p>
                <p>{this.state.dataFromIndirectComponent}</p>
            </div>
        )
    }
}

export default NestedLOneB;