import { BehaviorSubject } from 'rxjs';

const dataPassSubscriber = new BehaviorSubject(0);

const messageService = {
    send : function (msg) {
        dataPassSubscriber.next(msg)
    }
}

export {
    messageService,
    dataPassSubscriber
}